package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type Tenant_Info struct {
	Tenant_id  string `json:"tenant_id,omitempty"`
	Tenant_key string `json:"tenant_key,omitempty"`
}

type Tenantstransfer struct {
	Tenants   map[string]string
	Writelock sync.Mutex
}

var (
	loginToken Tenantstransfer
	closekafka chan int
	readokay   chan int
)

type TenantIngress struct {
	KafkaTenantConsumer   *kafka.Consumer
	KafkaSecPol           string
	KafkaBootstrapServers string
	TenantInfo            Tenant_Info
	TenantsTopicName      string
}

var TenantRetrieval TenantIngress

func (ti *TenantIngress) TIinit() {
	ti.TenantInfo = Tenant_Info{}
	ti.KafkaSecPol = "ssl"
	ti.KafkaBootstrapServers = "kafka1.coredge.ai:9094,kafka2.coredge.ai:9094"
	ti.TenantsTopicName = "tenants"
}
func (ti *TenantIngress) GetTenantInfo(readokay chan int, closekafka chan int) int {

	var err error
	ti.KafkaTenantConsumer, err = kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":               ti.KafkaBootstrapServers,
		"group.id":                        "trygroup23",
		"security.protocol":               ti.KafkaSecPol,
		"auto.offset.reset":               "earliest",
		"go.application.rebalance.enable": true,
		"enable.auto.commit":              false,
	})
	if err != nil {
		fmt.Println(err)
	}
	topics := []string{ti.TenantsTopicName}
	err = ti.KafkaTenantConsumer.SubscribeTopics(topics, nil)
	if err != nil {
		fmt.Println(err)
	}
	counter := 0 //counter for debugging
	run := true
	for run == true {
		//	fmt.Println("Count of loop ", counter)
		counter++
		select {
		case closesignal := <-closekafka:
			fmt.Printf("Received message to close consumer %v\n", closesignal)
			ti.KafkaTenantConsumer.Close()
			return 1
		default:
			run = run
		}

		ev := ti.KafkaTenantConsumer.Poll(0)
		if ev == nil {
			if len(loginToken.Tenants) > 0 {

				fmt.Println("might be the end of messages")
				//consume.Pause()
				readokay <- 1
			}
			continue
		}
		switch e := ev.(type) {
		case *kafka.Message:
			fmt.Println(string(e.Value))
			//	fmt.Println(e.Value)
			err_ := json.Unmarshal(e.Value, &ti.TenantInfo)
			if err_ != nil {
				fmt.Printf("Failed to unmarshal %v ", err_)
			}
			loginToken.Writelock.Lock()
			loginToken.Tenants[ti.TenantInfo.Tenant_id] = ti.TenantInfo.Tenant_key

			loginToken.Writelock.Unlock()
		/*	readokay <- 1
		 */
		case kafka.PartitionEOF:
			endofp := ev.(kafka.PartitionEOF)
			fmt.Printf("Got to the end of partition %v on topic %v\n", endofp.Partition, string(*endofp.Topic))
		case kafka.Error:
			fmt.Fprintf(os.Stderr, "%% Error: %v\n", e)
			run = false
		default:
			fmt.Printf("Ignored %v\n", e)
		}

	}
	return 1
}

func BEServerInit() error {
	TenantRetrieval = TenantIngress{}
	TenantRetrieval.TIinit()
	go TenantRetrieval.GetTenantInfo(readokay, closekafka)

	//beServerInit("", "")
	//go tokenManager
	return nil
}
func main() {

	loginToken.Tenants = make(map[string]string)
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)
	closekafka = make(chan int, 1)
	//	tenantinfo := make([]Tenant_Info, 10)
	readokay = make(chan int, 1)

	BEServerInit()
	var goon bool = true
	for goon {
		select {
		case sig := <-sigchan:
			{
				closekafka <- 1
				fmt.Printf("Caught signal %v: terminating\n", sig)
				goon = false
			}
		default:
			goon = goon
		}
		//spin lock
		select {
		case <-readokay:
			{
				loginToken.Writelock.Lock()
				//anything that needs to be done with the value
				fmt.Println("\nEntry from main ", loginToken.Tenants)
				loginToken.Writelock.Unlock()
				goto TEST
			}
		default:
			goon = goon
		}
		time.Sleep(1000)

	}
TEST:
	closekafka <- 1
	time.Sleep(5 * time.Second)
	fmt.Println("Map received at the end in main ", loginToken.Tenants, " with length ", len(loginToken.Tenants))
}
