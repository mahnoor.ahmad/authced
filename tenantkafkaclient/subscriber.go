package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/confluentinc/confluent-kafka-go/kafka"
)

type Tenant_Info struct {
	Tenant_id  string `json:"tenant_id,omitempty"`
	Tenant_key string `json:"tenant_key,omitempty"`
}

type tenants map[string]string

var (
	TenantMap tenants
)

func main() {

	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGINT, syscall.SIGTERM)
	closekafka := make(chan int, 1)
	//	tenantinfo := make([]Tenant_Info, 10)
	TenantMapCopy := make(map[string]string)
	TenantMap = make(map[string]string)
	tenantchannel := make(chan map[string]string, 1)
	tenantinfo := Tenant_Info{}
	consume, err := kafka.NewConsumer(&kafka.ConfigMap{
		"bootstrap.servers":               "kafka1.coredge.ai:9094",
		"group.id":                        "trygroup124",
		"security.protocol":               "ssl",
		"auto.offset.reset":               "earliest",
		"go.application.rebalance.enable": true,
		"enable.auto.commit":              false,
	})

	fmt.Println(err)

	topics := []string{"tenants"}
	err = consume.SubscribeTopics(topics, nil)
	fmt.Println(err)
	counter := 0
	run := true
	go func() int {
		for run == true {
			/*	select {
				case sig := <-sigchan:
					fmt.Printf("Caught signal %v: terminating\n", sig)
					run = false
				default:
					run = run
				}*/
			select {
			case closesignal := <-closekafka:
				fmt.Printf("Received message to close consumer %v\n", closesignal)
				consume.Close()
				return 1
			default:
				run = run
			}

			ev := consume.Poll(100)
			if ev == nil {
				continue
			}
			switch e := ev.(type) {
			case *kafka.Message:
				fmt.Println(string(e.Value))
				fmt.Println(e.Value)
				if len(e.Value) <= 1 {
					fmt.Println("No more messages beyond ", TenantMapCopy)
					//pause the routine and send values out (?)
				} else {
					err_ := json.Unmarshal(e.Value, &tenantinfo)
					if err_ != nil {
						fmt.Printf("Failed to unmarshal %v ", err_)
					}
					TenantMapCopy[tenantinfo.Tenant_id] = tenantinfo.Tenant_key
					counter++
					fmt.Println("Count of loop ", counter)
					//fmt.Printf("%#v", TenantMapCopy)
					tenantchannel <- TenantMapCopy
				}

			case kafka.Error:
				fmt.Fprintf(os.Stderr, "%% Error: %v\n", e)
				run = false
			default:
				fmt.Printf("Ignored %v\n", e)
			}

		}
		// fmt.Printf("Closing consumer\n")
		// consume.Close()
		return 0
	}()

	var goon bool = true
	for goon {
		select {
		case sig := <-sigchan:
			{
				closekafka <- 1
				fmt.Printf("Caught signal %v: terminating\n", sig)
				goon = false
			}
		default:
			goon = goon
		}
		select {
		case TenantMap = <-tenantchannel:
			fmt.Println("\nEntry from main ", TenantMap)

		default:
			goon = goon
		}
		time.Sleep(1000)

	}
	time.Sleep(5 * time.Second)
	fmt.Println("Map received at the end in main ", TenantMap, " with length ", len(TenantMap))
}
