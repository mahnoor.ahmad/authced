# Notes

We will use this directory to modify our kafka client and then test kafka specific code before we integrate it into the goroutine we will put in the ced-connector server/main.go

# Local kafka

We will do an on-the-surface replication of what we expect from the aws kafka cluster of the cloud team. Here is the link to set up a local kafka that we are using.
We will need Java jre for this.
https://adityasridhar.com/posts/how-to-easily-install-kafka-without-zookeeper

Running this in daemon mode did not work for me so I removed the `-daemon` flag. Please also ignore the need for multiple brokers/servers. We only need one for this level of testing.
Unless messages received change if we add more bootstrap servers into the ConfigMap, which is looking likely for now. In that case we will just have another file like `server.properties`
